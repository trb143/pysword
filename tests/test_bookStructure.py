# -*- coding: utf-8 -*-
###############################################################################
# PySword - A native Python reader of the SWORD Project Bible Modules         #
# --------------------------------------------------------------------------- #
# Copyright (c) 2008-2018 Various developers:                                 #
# Kenneth Arnold, Joshua Gross, Tomas Groth, Ryan Hiebert, Philip Ridout,     #
# Matthew Wardrop                                                             #
# --------------------------------------------------------------------------- #
# This program is free software; you can redistribute it and/or modify it     #
# under the terms of the GNU General Public License as published by the Free  #
# Software Foundation; version 2 of the License.                              #
#                                                                             #
# This program is distributed in the hope that it will be useful, but WITHOUT #
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       #
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for    #
# more details.                                                               #
#                                                                             #
# You should have received a copy of the GNU General Public License along     #
# with this program; if not, write to the Free Software Foundation, Inc., 51  #
# Franklin St, Fifth Floor, Boston, MA 02110-1301 USA                         #
###############################################################################

from tests import TestCase

from pysword.books import BookStructure


class TestBookStructure(TestCase):
    def test_get_indicies_invalid_chapter(self):
        """
        Test that get_indicies() throws exception on invalid chapter
        """
        # GIVEN: A test BookStructure with 4 chapters
        structure = BookStructure(u'test name', u'test osis name', u'test abbr', [31, 25, 24, 26])

        # WHEN: Calling get_indicies() with an invalid chapter parameter
        # THEN: An ValueError exception should be thrown
        self.assertRaises(ValueError, structure.get_indicies,  5, 1)

    def test_get_indicies_invalid_verse(self):
        """
        Test that get_indicies() throws exception on invalid verse
        """
        # GIVEN: A test BookStructure with 4 chapters
        structure = BookStructure(u'test name', u'test osis name', u'test abbr', [31, 25, 24, 26])

        # WHEN: Calling get_indicies() with an invalid verse parameter
        # THEN: An ValueError exception should be thrown
        self.assertRaises(ValueError, structure.get_indicies,  3, 25)
