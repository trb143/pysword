# -*- coding: utf-8 -*-
###############################################################################
# PySword - A native Python reader of the SWORD Project Bible Modules         #
# --------------------------------------------------------------------------- #
# Copyright (c) 2008-2018 Various developers:                                 #
# Kenneth Arnold, Joshua Gross, Tomas Groth, Ryan Hiebert, Philip Ridout,     #
# Matthew Wardrop                                                             #
# --------------------------------------------------------------------------- #
# This program is free software; you can redistribute it and/or modify it     #
# under the terms of the GNU General Public License as published by the Free  #
# Software Foundation; version 2 of the License.                              #
#                                                                             #
# This program is distributed in the hope that it will be useful, but WITHOUT #
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       #
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for    #
# more details.                                                               #
#                                                                             #
# You should have received a copy of the GNU General Public License along     #
# with this program; if not, write to the Free Software Foundation, Inc., 51  #
# Franklin St, Fifth Floor, Boston, MA 02110-1301 USA                         #
###############################################################################

import os

from tests.test_utils import TestCase

from pysword.modules import SwordModules

TEST_RESOURCE_FOLDER = os.path.join(os.path.dirname(os.path.realpath(__file__)), u'resources')


class TestModules(TestCase):

    def test_load_finpr_zip(self):
        """
        Test that the FinPR.zip file is loaded correctly.
        """
        # GIVEN: The FinPR.zip file
        modules = SwordModules(os.path.join(TEST_RESOURCE_FOLDER, u'FinPR.zip'))

        # WHEN: Parsing the FinPR module and reading a passage.
        found_modules = modules.parse_modules()
        bible = modules.get_bible_from_module(u'FinPR')
        output = bible.get(books=[u'john'], chapters=[3], verses=[16])

        # THEN: The FinPR module should be the only one found. And the passage should be equal to the known text.
        assert u'FinPR' in found_modules
        assert len(found_modules) == 1
        assert output == u'Sillä niin on Jumala maailmaa rakastanut, että hän antoi ainokaisen Poikansa, ' \
                         u'ettei yksikään, joka häneen uskoo, hukkuisi, vaan hänellä olisi iankaikkinen elämä.'

    def test_load_chipinyin_zip(self):
        """
        Test that the chipinyin.zip file is loaded correctly.
        """
        # GIVEN: The chipinyin.zip file
        modules = SwordModules(os.path.join(TEST_RESOURCE_FOLDER, u'chipinyin.zip'))

        # WHEN: Parsing the chipinyin module and reading a passage.
        found_modules = modules.parse_modules()
        bible = modules.get_bible_from_module(u'ChiPinyin')
        output = bible.get(books=[u'john'], chapters=[3], verses=[16])

        # THEN: The ChiPinyin module should be the only one found. And the passage should be equal to the known text.
        assert u'ChiPinyin' in found_modules
        assert len(found_modules) == 1
        assert output == u' Shén aì shìrén , shènzhì jiāng tāde dú shēng zǐ cìgĕi tāmen , jiào yīqiè xìn tāde , bú ' \
                         u'zhì mièwáng , fǎn dé yǒngshēng . '

    def test_load_bsv_zip(self):
        """
        Test that the bsv.zip file is loaded correctly.
        """
        # GIVEN: The bsv.zip file
        modules = SwordModules(os.path.join(TEST_RESOURCE_FOLDER, u'bsv.zip'))

        # WHEN: Parsing the chipinyin module and reading a passage.
        found_modules = modules.parse_modules()
        bible = modules.get_bible_from_module(u'BSV')
        output = bible.get(books=[u'john'], chapters=[3], verses=[16])

        # THEN: The BSV module should be the only one found. And the passage should be equal to the known text.
        assert u'BSV' in found_modules
        assert len(found_modules) == 1
        assert output == u'For God so loved the world, that he gave his only begotten Son, that whoever believes in ' \
                         u'him should not perish, but have everlasting life.'
